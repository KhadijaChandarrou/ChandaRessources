**Thématique:** Traitement de texte ***
**Notions liées:** ***
**Résumé de l’activité:** Mise en forme d'un document en traitement de texte ***
**Objectifs:** Mise en forme des caractères et paragraphe,insertion d'objets: insertion d'images, des formes, wordArt, tableau... ***
**Auteur:**Chandarrou ***
**Durée de l’activité:**2H ***
**Forme de participation:** individuelle. ***
**Matériel nécessaire:** Ordinateurs.
**Préparation:** ***
**Autres références :** ***
**Fiche élève:**Disponible [ici] (lien)
